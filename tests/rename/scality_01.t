#!/bin/sh

desc="rename succeeds if user has write permissions on both parent directories"

dir=`dirname $0`
. ${dir}/../misc.sh

echo "1..184"

n0=`namegen`
n1=`namegen`
n2=`namegen`
n3=`namegen`

# work in ${n3}
expect 0 mkdir ${n3} 0755
cdir=`pwd`
cd ${n3}

expect 0 mkdir ${n0} 0755


# User owns the directory and has no write permission on the file/directory to be renamed
# 3
expect 0 chown ${n0} 65534 65533
expect 0 chmod ${n0} 0300
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# User owns the directory but no write permission on it
# 11
expect 0 chmod ${n0} 0500
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}

# User is owner with no write permission but is in group with write permission
# 18
expect 0 chmod ${n0} 0530
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}

# User is owner with no write permission but other has write permission
# 25
expect 0 chmod ${n0} 0503
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}



# User is in the group owning the directory but is not owner and has no write
# permission on the file/directory to be renamed
# 32
expect 0 chmod ${n0} 0230
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Still the case if owner has not the rights to do it
# 39
expect 0 chmod ${n0} 0030
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}


# Now failing if group has no write permission on the directory
# 46
expect 0 chmod ${n0} 0050
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}


# User is in group with no write permission but other has write permission
# 53
expect 0 chmod ${n0} 0052
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}


# User is not owner nor in group but has write permission on the directory
# 60
expect 0 chmod ${n0} 0203
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Still the case if owner has not the rights to do it
# 67
expect 0 chmod ${n0} 0003
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Now failing if other has no write permission on the directory
# 74
expect 0 chmod ${n0} 0000
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}


###
# Same tests but destination directory is different from source directory
###

expect 0 mkdir ${n2} 0777

# User owns the directory and has no write permission on the file to be moved
# 82
expect 0 chown ${n0} 65534 65533
expect 0 chmod ${n0} 0300
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# User owns the directory but no write permission on it
# 92
expect 0 chmod ${n0} 0500
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}

# User is owner with no write permission but is in group with write permission
# 101
expect 0 chmod ${n0} 0530
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}

# User is owner with no write permission but other has write permission
# 110
expect 0 chmod ${n0} 0503
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}



# User is in the group owning the directory but is not owner and has no write
# permission on the file/directory to be renamed
# 119
expect 0 chmod ${n0} 0230
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Still the case if owner has not the rights to do it
# 128
expect 0 chmod ${n0} 0030
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}


# Now failing if group has no write permission on the directory
# 137
expect 0 chmod ${n0} 0050
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}


# User is in group with no write permission but other has write permission
# 146
expect 0 chmod ${n0} 0052
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}


# User is not owner nor in group but has write permission on the directory
# 155
expect 0 chmod ${n0} 0203
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Still the case if owner has not the rights to do it
# 164
expect 0 chmod ${n0} 0003
expect 0 create ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Now failing if other has no write permission on the directory
# 173
expect 0 chmod ${n0} 0000
expect 0 create ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}

expect 0 rmdir ${n0}
expect 0 rmdir ${n2}

cd ${cdir}
expect 0 rmdir ${n3}
