#!/bin/sh
# $FreeBSD: .../fstest/tests/rename/21.t,v 1.1 2015/05/07

desc="ctime update after rename"

dir=`dirname $0`
. ${dir}/../misc.sh

echo "1..52"

n0=`namegen`
n1=`namegen`
n2=`namegen`
n3=`namegen`
n4=`namegen`

expect 0 mkdir ${n3} 0755
cdir=`pwd`
cd ${n3}

# successful rename in same directory: ctime updated
expect 0 create ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n1} ctime`
test_check $ctime1 -lt $ctime2

expect 0 unlink ${n1}

expect 0 mkdir ${n0} 0755
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 rmdir ${n1}

expect 0 mkfifo ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 unlink ${n1}

expect 0 symlink ${n2} ${n0}
ctime1=`${fstest} lstat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n1}
ctime2=`${fstest} lstat ${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 unlink ${n1}

# successful rename in different directories: ctime updated
expect 0 mkdir ${n4} 0755
expect 0 create ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n4}/${n1}
ctime2=`${fstest} stat ${n4}/${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 unlink ${n4}/${n1}

expect 0 mkdir ${n0} 0755
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n4}/${n1}
ctime2=`${fstest} stat ${n4}/${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 rmdir ${n4}/${n1}

expect 0 mkfifo ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n4}/${n1}
ctime2=`${fstest} stat ${n4}/${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 unlink ${n4}/${n1}

expect 0 symlink ${n2} ${n0}
ctime1=`${fstest} lstat ${n0} ctime`
sleep 1
expect 0 rename ${n0} ${n4}/${n1}
ctime2=`${fstest} lstat ${n4}/${n1} ctime`
test_check $ctime1 -lt $ctime2
expect 0 unlink ${n4}/${n1}
expect 0 rmdir ${n4}

# unsuccessful rename does not update ctime.
expect 0 create ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect EACCES -u 65534 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n0} ctime`
test_check $ctime1 -eq $ctime2
expect 0 unlink ${n0}

expect 0 mkdir ${n0} 0755
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect EACCES -u 65534 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n0} ctime`
test_check $ctime1 -eq $ctime2
expect 0 rmdir ${n0}

expect 0 mkfifo ${n0} 0644
ctime1=`${fstest} stat ${n0} ctime`
sleep 1
expect EACCES -u 65534 rename ${n0} ${n1}
ctime2=`${fstest} stat ${n0} ctime`
test_check $ctime1 -eq $ctime2
expect 0 unlink ${n0}

expect 0 symlink ${n2} ${n0}
ctime1=`${fstest} lstat ${n0} ctime`
sleep 1
expect EACCES -u 65534 rename ${n0} ${n1}
ctime2=`${fstest} lstat ${n0} ctime`
test_check $ctime1 -eq $ctime2
expect 0 unlink ${n0}

cd ${cdir}
expect 0 rmdir ${n3}
