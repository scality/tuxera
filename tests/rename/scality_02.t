#!/bin/sh

desc="rename needs write permission on destination directory when deleting entries there"

dir=`dirname $0`
. ${dir}/../misc.sh

echo "1..252"

n0=`namegen`
n1=`namegen`
n2=`namegen`
n3=`namegen`

# work in ${n3}
expect 0 mkdir ${n3} 0755
cdir=`pwd`
cd ${n3}

expect 0 mkdir ${n0} 0777


###
# Source and parent directories are the same
###



# User owns the directory and has no write permission on the file/directory to be overwritten
# 3
echo "ntest:$ntest, 3"
expect 0 chown ${n0} 65534 65533
expect 0 chmod ${n0} 0300
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# User owns the directory but no write permission on it
# 13
echo "ntest:$ntest, 13"
expect 0 chmod ${n0} 0500
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}

# User is owner with no write permission but is in group with write permission
# 24
echo "ntest:$ntest, 24"
expect 0 chmod ${n0} 0530
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}

# User is owner with no write permission but other has write permission
# 35
echo "ntest:$ntest, 35"
expect 0 chmod ${n0} 0503
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}



# User is in the group owning the directory but is not owner and has no write
# permission on the file/directory to be renamed
# 46
echo "ntest:$ntest, 46"
expect 0 chmod ${n0} 0230
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Still the case if owner has not the rights to do it
# 55
echo "ntest:$ntest, 55"
expect 0 chmod ${n0} 0030
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}


# Now failing if group has no write permission on the directory
# 64
echo "ntest:$ntest, 64"
expect 0 chmod ${n0} 0050
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}


# User is in group with no write permission but other has write permission
# 75
echo "ntest:$ntest, 75"
expect 0 chmod ${n0} 0052
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}


# User is not owner nor in group but has write permission on the directory
# 86
echo "ntest:$ntest, 86"
expect 0 chmod ${n0} 0203
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Still the case if owner has not the rights to do it
# 95
echo "ntest:$ntest, 95"
expect 0 chmod ${n0} 0003
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n2}

# Now failing if other has no write permission on the directory
# 104
echo "ntest:$ntest, 104"
expect 0 chmod ${n0} 0000
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n0}/${n2} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n0}/${n2}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n0}/${n2}


#####
### Same tests but destination directory is different from source directory
#####

expect 0 mkdir ${n2} 0777

# User owns the directory and has no write permission on the file to be moved
# 116
echo "ntest:$ntest, 116"
expect 0 chown ${n0} 65534 65533
expect 0 chmod ${n0} 0300
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# User owns the directory but no write permission on it
# 128
echo "ntest:$ntest, 128"
expect 0 chmod ${n0} 0500
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}

# User is owner with no write permission but is in group with write permission
# 141
echo "ntest:$ntest, 141"
expect 0 chmod ${n0} 0530
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n0}/${n2} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n0}/${n2}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}

# User is owner with no write permission but other has write permission
# 154
echo "ntest:$ntest, 154"
expect 0 chmod ${n0} 0503
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65534 -g 65534 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}



# User is in the group owning the directory but is not owner and has no write
# permission on the file/directory to be renamed
# 167
echo "ntest:$ntest, 167"
expect 0 chmod ${n0} 0230
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Still the case if owner has not the rights to do it
# 178
echo "ntest:$ntest, 178"
expect 0 chmod ${n0} 0030
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}


# Now failing if group has no write permission on the directory
# 189
echo "ntest:$ntest, 189"
expect 0 chmod ${n0} 0050
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}


# User is in group with no write permission but other has write permission
# 202
echo "ntest:$ntest, 202"
expect 0 chmod ${n0} 0052
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65533 -g 65533 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}


# User is not owner nor in group but has write permission on the directory
# 215
echo "ntest:$ntest, 215"
expect 0 chmod ${n0} 0203
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Still the case if owner has not the rights to do it
# 226
echo "ntest:$ntest, 226"
expect 0 chmod ${n0} 0003
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect 0 -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n2}/${n1}

# Now failing if other has no write permission on the directory
# 237
echo "ntest:$ntest, 237"
expect 0 chmod ${n0} 0000
expect 0 create ${n0}/${n1} 0000
expect 0 create ${n2}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 unlink ${n0}/${n1}
expect 0 unlink ${n2}/${n1}
# with a directory (needs write on it to update '..')
expect 0 mkdir ${n0}/${n1} 0000
expect 0 mkdir ${n2}/${n1} 0000
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 chmod ${n0}/${n1} 0002
expect EACCES -u 65532 -g 65532 rename ${n0}/${n1} ${n2}/${n1}
expect 0 rmdir ${n0}/${n1}
expect 0 rmdir ${n2}/${n1}

expect 0 rmdir ${n0}
expect 0 rmdir ${n2}

cd ${cdir}
expect 0 rmdir ${n3}
