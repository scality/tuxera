#!/bin/sh

desc="rmdir succeeds if user has write permissions on the parent directory"

dir=`dirname $0`
. ${dir}/../misc.sh

echo "1..34"

n0=`namegen`
n1=`namegen`
n2=`namegen`

# work in ${n2}
expect 0 mkdir ${n2} 0755
cdir=`pwd`
cd ${n2}

expect 0 mkdir ${n0} 0755

# User owns the directory and has no write permission on the file to be removed
# 3
expect 0 chown ${n0} 65534 65533
expect 0 chmod ${n0} 0300
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65534 -g 65534 rmdir ${n0}/${n1}
# User owns the directory but no write permission on it
# 7
expect 0 mkdir ${n0}/${n1} 0000
expect 0 chmod ${n0} 0500
expect EACCES -u 65534 -g 65534 rmdir ${n0}/${n1}
# User is owner with no write permission but is in group with write permission
# 10
expect 0 chmod ${n0} 0530
expect EACCES -u 65534 -g 65533 rmdir ${n0}/${n1}
# User is owner with no write permission but other has write permission
# 12
expect 0 chmod ${n0} 0503
expect EACCES -u 65534 -g 65533 rmdir ${n0}/${n1}


# User is in the group owning the directory but is not owner and has no write permission on the file to be removed
# 14
expect 0 chmod ${n0} 0230
expect 0 -u 65533 -g 65533 rmdir ${n0}/${n1}
# Still the case if owner has not the rights to do it
# 16
expect 0 mkdir ${n0}/${n1} 0000
expect 0 chmod ${n0} 0030
expect 0 -u 65533 -g 65533 rmdir ${n0}/${n1}
# Now failing if group has no write permission on the directory
# 19
expect 0 mkdir ${n0}/${n1} 0000
expect 0 chmod ${n0} 0050
expect EACCES -u 65533 -g 65533 rmdir ${n0}/${n1}
# User is in group with no write permission but other has write permission
# 22
expect 0 chmod ${n0} 0052
expect EACCES -u 65533 -g 65533 rmdir ${n0}/${n1}


# User is not owner nor in group but has write permission on the directory
# 24
expect 0 chmod ${n0} 0203
expect 0 -u 65532 -g 65532 rmdir ${n0}/${n1}
# Still the case if owner has not the rights to do it
# 26
expect 0 chmod ${n0} 0003
expect 0 mkdir ${n0}/${n1} 0000
expect 0 -u 65532 -g 65532 rmdir ${n0}/${n1}
# Now failing if other has no write permission on the directory
# 29
expect 0 mkdir ${n0}/${n1} 0000
expect 0 chmod ${n0} 0000
expect EACCES -u 65532 -g 65532 rmdir ${n0}/${n1}

expect 0 rmdir ${n0}/${n1}

expect 0 rmdir ${n0}

cd ${cdir}
expect 0 rmdir ${n2}
